# Ofac

This applications has a bunch of rest services around the OFAC

## Configuration

### Docker

Create sdn-network

```bash
    docker network create ofac-network
```

### PostgreSQL

```bash
    docker run -d -p 5432:5432 --name postgresql-container --net ofac-network -e POSTGRES_USER=su -e POSTGRES_PASSWORD=pw -e POSTGRES_DB=ofac --ulimit memlock=-1:-1 -it --memory-swappiness=0 --restart unless-stopped  postgres:latest
```

```bash
    liquibase --logLevel debug --classpath=./lib/postgresql-42.3.1.jar --username=$OFAC_DB_USER --password=$OFAC_DB_PASS  --url=jdbc:postgresql://$OFAC_DB_HOST:$OFAC_DB_PORT/ofac --changeLogFile=sdn.postgresql.sql generateChangeLog
```

This project uses some full text search features in postgreSQL. You will need to enable the follow extensions

``
    CREATE EXTENSION pg_trgm;

``

## Ofac Sdn

[sdn_advanced.xsd](https://www.treasury.gov/ofac/downloads/sanctions/1.0/sdn_advanced.xsd)

### Ftp

```url
    ftp://ofacftp.treas.gov/
```

#### Sdn Advanced

[sdn_advanced_notes.pdf](https://home.treasury.gov/system/files/126/sdn_advanced_notes.pdf)

## Environment Variables

```bash
    export OFAC_DB_USER=
    export OFAC_DB_PASS=
    export OFAC_DB_HOST=
    export OFAC_DB_PORT=
    export OFAC_SDN_DIRECTORY=

    export OFAC_FTP_HOST=
    export OFAC_FTP_PORT=
```

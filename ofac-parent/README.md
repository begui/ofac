# ofac-parent

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Packaging and running the application

The application can be packaged using:

```shell script
    ./mvnw clean package -Dquarkus.package.type=uber-jar
```
The project is automatically set to build an uber jar so the 'quarkus.package.type' is optional

## Creating a native executable

You can create a native executable using:
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```


## ofac-cli

This project uses liquibase to build the schema. Run the following command to do so. You can pass a '-d' option to
the end to drop and recreate. Please note the option will destroy everything

```shell script
    ./mvnw -pl ofac-cli compile quarkus:dev -Dquarkus.args="ofacDbSync -d"
```
You can populate the db schema by running one of the following

```shell script
    ./mvnw -pl ofac-cli compile quarkus:dev -Dquarkus.args="ofacSdn -e=fetch"
    ./mvnw -pl ofac-cli compile quarkus:dev -Dquarkus.args="ofacSdn -e=populate"
    ./mvnw -pl ofac-cli compile quarkus:dev -Dquarkus.args="ofacSdn -e=fetchAndPopulate"
```


Alternatively, You can run the following command with the following execution option against the built jar

```shell script

    java -jar ofac-cli-*-runner.jar ofacDbSync -d
    java -jar ofac-cli-*-runner.jar ofacSdn -e=fetch
    java -jar ofac-cli-*-runner.jar ofacSdn -e=populate
    java -jar ofac-cli-*-runner.jar ofacSdn -e=fetchAndPopulate
```

## ofac-service

This project has a few services for fetching the sdn information. It uses the older blocking ways of doing things

Run this in 'dev' mode the quarkus way

```shell script
    ./mvnw -pl ofac-service compile quarkus:dev
````

## Issues

- I'm having a hard time to do what I want to do with quarkus and maven.

    https://github.com/quarkusio/quarkus/issues/6266

- Adding 'passiveMode' to true since this is sftp. Fedora server complains.

    https://stackoverflow.com/questions/11345926/apache-camel-failing-ftp-component/33300963

- ofac-service fails to build the native binary after adding the new java record code. See tracked issue

    https://github.com/quarkusio/quarkus/issues/20891

## Related guides

- Picocli ([guide](https://quarkus.io/guides/picocli)): Develop command line applications with Picocli

[Related guide section...](https://quarkus.io/guides/picocli#command-line-application-with-multiple-commands)



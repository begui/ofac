package com.begui.ofac.db.sdn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
//import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SDN_Id")
@NamedQuery(name = "SdnId.getByUid", query = "SELECT sdnId FROM SdnId sdnId, SdnEntry s where sdnId.sdnEntry = s and sdnId.uid = :uid and s.publishInfo.publishDate = ("+ PublishInfo.LATESTPUBLISHED + ")")
@NamedQuery(name = "SdnId.getIdsBySdnEntryUid", query = "SELECT sdnId FROM SdnId sdnId, SdnEntry s where sdnId.sdnEntry = s and s.uid = :uid and s.publishInfo.publishDate = ("+ PublishInfo.LATESTPUBLISHED + ")")
public class SdnId {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_id_generator")
	@SequenceGenerator(name = "sdn_id_generator", sequenceName = "sdn_id_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;

	public int uid;
	public String idType;
	public String idNumber;
	public String idCountry;
	// this is not a consistent date format, treat it as a string for now
	public String issueDate;
	// this is not a consistent date format, treat it as a string for now
	public String expirationDate;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name ="SDNENTRY_ID", referencedColumnName = "ID")
	public SdnEntry sdnEntry;

}

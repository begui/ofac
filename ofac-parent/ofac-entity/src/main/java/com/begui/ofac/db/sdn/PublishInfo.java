package com.begui.ofac.db.sdn;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "SDN_PublishInfo", indexes = {
		@Index(name = "fn_index_publishinfo_date", columnList = "publishDate", unique = true) })
@NamedQuery(name = "PublishInfo.getLatestByPublishDate", query = "from PublishInfo p where p.publishDate = ("+ PublishInfo.LATESTPUBLISHED + ")")
@NamedQuery(name = "PublishInfo.getByPublishDate", query = "from PublishInfo p where p.publishDate = :publishDate")
public class PublishInfo {

	public static final String LATESTPUBLISHED = "select max(publishInfo.publishDate) from PublishInfo publishInfo";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_publishinfo_generator")
	@SequenceGenerator(name = "sdn_publishinfo_generator", sequenceName = "sdn_publishinfo_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;
	@NotNull
	public LocalDate publishDate;
	@NotNull
	public Integer recordCount;
	@JsonIgnore
	@OneToMany(mappedBy = "publishInfo", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action=OnDeleteAction.CASCADE) 
	public List<SdnEntry> sdnEntry;
}

package com.begui.ofac.db.sdn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "Program")
@Table(name = "SDN_Program")
@NamedQuery(name = "Program.findByName", query = "from Program p where p.name = :name")
public class Program {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_program_generator")
	@SequenceGenerator(name = "sdn_program_generator", sequenceName = "sdn_program_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;

	@Column(name = "name", unique = true, updatable = false)
	public String name;

}

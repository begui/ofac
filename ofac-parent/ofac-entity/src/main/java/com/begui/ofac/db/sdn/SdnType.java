package com.begui.ofac.db.sdn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity(name = "SdnType")
@Table(name = "SDN_Type")
@NamedQuery(name = "SdnType.findByName", query = "from SdnType t where t.name = :name")
public class SdnType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_type_generator")
	@SequenceGenerator(name = "sdn_type_generator", sequenceName = "sdn_type_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;

	@Column(name = "name", unique = true, updatable = false)
	public String name;

}

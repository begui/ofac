package com.begui.ofac.db.sdn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SDN_VesselInfo")
public class VesselInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_vessel_info_generator")
	@SequenceGenerator(name = "sdn_vessel_info_generator", sequenceName = "sdn_vessel_info_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;

	public String callSign;
	public String vesselType;
	public String vesselFlag;
	public String vesselOwner;
	public Integer tonnage;
	public Integer grossRegisteredTonnage;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "SDNENTRY_ID", referencedColumnName = "ID")
	@OnDelete(action=OnDeleteAction.CASCADE) 
	public SdnEntry sdnEntry;
}

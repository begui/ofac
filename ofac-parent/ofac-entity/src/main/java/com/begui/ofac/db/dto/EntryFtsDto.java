package com.begui.ofac.db.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class EntryFtsDto {

	public int uid;
	public String firstName;
	public String lastName;
	
	public EntryFtsDto(int uid, String firstName, String lastName) {
		this.uid = uid;
        this.firstName = firstName;
        this.lastName = lastName;
	}
}

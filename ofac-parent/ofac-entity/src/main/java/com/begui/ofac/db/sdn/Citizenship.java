package com.begui.ofac.db.sdn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SDN_Citizenship")
public class Citizenship {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "sdn_citizenship_generator")
	@SequenceGenerator(name = "sdn_citizenship_generator", sequenceName = "sdn_citizenship_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;

	public int uid;
	@NotNull
    public String country;
    public boolean mainEntry;
    @JsonIgnore
	@ManyToOne
	@JoinColumn(name ="SDNENTRY_ID", referencedColumnName = "ID")
	public SdnEntry sdnEntry;

}

package com.begui.ofac.db.sdn;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "SDN_Entry", indexes = {
		@Index(name = "fn_index_publishInfo", columnList = "uid,publishInfo_Id", unique = true) })
@NamedQuery(name = "SdnEntry.getAllByUid", query = "from SdnEntry s where s.uid = :uid")
@NamedQuery(name = "SdnEntry.getByUid", query = "select s from SdnEntry s, PublishInfo p where s.publishInfo = p and s.uid = :uid and p.publishDate = (select max(publishInfo.publishDate) from PublishInfo publishInfo)")
public class SdnEntry {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_entry_generator")
	@SequenceGenerator(name = "sdn_entry_generator", sequenceName = "sdn_entry_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;

	@ManyToOne(targetEntity = PublishInfo.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "publishInfo_Id", nullable = false)
	public PublishInfo publishInfo;
	public int uid;
	public String firstName;
	@NotNull
	public String lastName;
	public String title;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sdnType_id")
	public SdnType sdnType;
	@Column(columnDefinition = "text")
	public String remarks;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "sdn_entry_program", //
			joinColumns = { //
					@JoinColumn(name = "sdnEntry_id", referencedColumnName = "id") }, //
			inverseJoinColumns = { //
					@JoinColumn(name = "program_id", referencedColumnName = "id") }, //
			uniqueConstraints = {})
	@OnDelete(action = OnDeleteAction.CASCADE)
	public Set<Program> programs = new HashSet<>();
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<SdnId> idList;
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<Aka> akaList;
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<Address> addressList;
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<Nationality> nationalityList;
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<Citizenship> citizenshipList;
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<DateOfBirth> dateOfBirthList;
	@OneToMany(mappedBy = "sdnEntry", cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public List<PlaceOfBirth> placeOfBirthList;

	@OneToOne(targetEntity = VesselInfo.class, cascade = CascadeType.ALL, orphanRemoval = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public VesselInfo vesselInfo;

}

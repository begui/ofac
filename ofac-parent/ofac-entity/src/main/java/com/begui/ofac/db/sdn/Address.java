package com.begui.ofac.db.sdn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SDN_Address")
@NamedQuery(name = "Address.getLatestByUid", query = "SELECT address FROM Address address, SdnEntry s where address.sdnEntry = s and address.uid = :uid and s.publishInfo.publishDate = ("+ PublishInfo.LATESTPUBLISHED + ")")
@NamedQuery(name = "Address.getLatestAddressBySdnEntryUid", query = "SELECT address FROM Address address, SdnEntry s where address.sdnEntry = s and s.uid = :uid and s.publishInfo.publishDate = ("+ PublishInfo.LATESTPUBLISHED + ")")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sdn_address_generator")
	@SequenceGenerator(name = "sdn_address_generator", sequenceName = "sdn_address_seq", allocationSize = 1)
	@Column(name = "id", updatable = false, nullable = false)
	public Long id;
	public int uid;
	public String address1;
	public String address2;
	public String address3;
	public String city;
	public String stateOrProvince;
	public String postalCode;
	public String country;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name ="SDNENTRY_ID", referencedColumnName = "ID")
	public SdnEntry sdnEntry;


	
}

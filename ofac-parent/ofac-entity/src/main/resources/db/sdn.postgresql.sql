-- liquibase formatted sql

-- changeset bj:1635654854049-1
CREATE TABLE IF NOT EXISTS camel_messageprocessed (id BIGINT NOT NULL, createdat TIMESTAMP WITHOUT TIME ZONE, messageid VARCHAR(255), processorname VARCHAR(255), CONSTRAINT camel_messageprocessed_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-2
CREATE TABLE IF NOT EXISTS sdn_entry (id BIGINT NOT NULL, firstname VARCHAR(255), lastname VARCHAR(255), remarks TEXT, title VARCHAR(255), uid INTEGER NOT NULL, publishinfo_id BIGINT NOT NULL, sdntype_id BIGINT, vesselinfo_id BIGINT, document_firstname_lastname TSVECTOR, CONSTRAINT sdn_entry_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-3
CREATE TABLE IF NOT EXISTS sdn_nationality (id BIGINT NOT NULL, country VARCHAR(255), mainentry BOOLEAN NOT NULL, uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_nationality_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-4
CREATE TABLE IF NOT EXISTS sdn_entry_program (sdnentry_id BIGINT NOT NULL, program_id BIGINT NOT NULL, CONSTRAINT sdn_entry_program_pkey PRIMARY KEY (sdnentry_id, program_id));

-- changeset bj:1635654854049-5
CREATE TABLE IF NOT EXISTS sdn_placeofbirth (id BIGINT NOT NULL, mainentry BOOLEAN NOT NULL, placeofbirth VARCHAR(255), uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_placeofbirth_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-6
CREATE TABLE IF NOT EXISTS sdn_citizenship (id BIGINT NOT NULL, country VARCHAR(255), mainentry BOOLEAN NOT NULL, uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_citizenship_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-7
CREATE TABLE IF NOT EXISTS sdn_aka (id BIGINT NOT NULL, category VARCHAR(255), firstname VARCHAR(255), lastname VARCHAR(255), type VARCHAR(255), uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_aka_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-8
CREATE TABLE IF NOT EXISTS sdn_program (id BIGINT NOT NULL, name VARCHAR(255), CONSTRAINT sdn_program_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-9
CREATE TABLE IF NOT EXISTS sdn_vesselinfo (id BIGINT NOT NULL, callsign VARCHAR(255), grossregisteredtonnage INTEGER, tonnage INTEGER, vesselflag VARCHAR(255), vesselowner VARCHAR(255), vesseltype VARCHAR(255), sdnentry_id BIGINT, CONSTRAINT sdn_vesselinfo_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-10
CREATE TABLE IF NOT EXISTS sdn_id (id BIGINT NOT NULL, expirationdate VARCHAR(255), idcountry VARCHAR(255), idnumber VARCHAR(255), idtype VARCHAR(255), issuedate VARCHAR(255), uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_id_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-11
CREATE TABLE IF NOT EXISTS sdn_address (id BIGINT NOT NULL, address1 VARCHAR(255), address2 VARCHAR(255), address3 VARCHAR(255), city VARCHAR(255), country VARCHAR(255), postalcode VARCHAR(255), stateorprovince VARCHAR(255), uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_address_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-12
CREATE TABLE IF NOT EXISTS sdn_dateofbirth (id BIGINT NOT NULL, dateofbirth VARCHAR(255), mainentry BOOLEAN NOT NULL, uid INTEGER NOT NULL, sdnentry_id BIGINT, CONSTRAINT sdn_dateofbirth_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-13
CREATE TABLE IF NOT EXISTS sdn_publishinfo (id BIGINT NOT NULL, publishdate date, recordcount INTEGER, CONSTRAINT sdn_publishinfo_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-14
CREATE TABLE IF NOT EXISTS sdn_type (id BIGINT NOT NULL, name VARCHAR(255), CONSTRAINT sdn_type_pkey PRIMARY KEY (id));

-- changeset bj:1635654854049-15
CREATE UNIQUE INDEX IF NOT EXISTS uk3gjvyad9j4kd1xeog8xws0skp ON camel_messageprocessed(processorname, messageid);

-- changeset bj:1635654854049-17
ALTER TABLE sdn_entry DROP CONSTRAINT IF EXISTS fn_index_publishinfo;
ALTER TABLE sdn_entry ADD CONSTRAINT fn_index_publishinfo UNIQUE (uid, publishinfo_id);

-- changeset bj:1635654854049-18
ALTER TABLE sdn_nationality DROP CONSTRAINT IF EXISTS fk1vpdg5e9xnqsedvo9ve8ug6e2;
ALTER TABLE sdn_nationality ADD CONSTRAINT fk1vpdg5e9xnqsedvo9ve8ug6e2 FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-19
ALTER TABLE sdn_entry_program DROP CONSTRAINT IF EXISTS fk6nohqi5r3mdpy63p2r15x0vkt;
ALTER TABLE sdn_entry_program ADD CONSTRAINT fk6nohqi5r3mdpy63p2r15x0vkt FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-20
ALTER TABLE sdn_placeofbirth DROP CONSTRAINT IF EXISTS fk8bf0x39th6k7tebd28e5ndrnb;
ALTER TABLE sdn_placeofbirth ADD CONSTRAINT fk8bf0x39th6k7tebd28e5ndrnb FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-21
ALTER TABLE sdn_citizenship DROP CONSTRAINT IF EXISTS fk8nblukinboaeomw2tkdonguwb;
ALTER TABLE sdn_citizenship ADD CONSTRAINT fk8nblukinboaeomw2tkdonguwb FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-22
ALTER TABLE sdn_aka DROP CONSTRAINT IF EXISTS fka7c4cd0h7y8bgvgrwlr6hjyrr;
ALTER TABLE sdn_aka ADD CONSTRAINT fka7c4cd0h7y8bgvgrwlr6hjyrr FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-23
ALTER TABLE sdn_program DROP CONSTRAINT IF EXISTS uk_e1n2xqmkvbvnvgw1nsf4va06x;
ALTER TABLE sdn_program ADD CONSTRAINT uk_e1n2xqmkvbvnvgw1nsf4va06x UNIQUE (name);

-- changeset bj:1635654854049-24
ALTER TABLE sdn_vesselinfo DROP CONSTRAINT IF EXISTS fkeii0vpf0x2ry46lufwxsex253;
ALTER TABLE sdn_vesselinfo ADD CONSTRAINT fkeii0vpf0x2ry46lufwxsex253 FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-25
ALTER TABLE sdn_id DROP CONSTRAINT IF EXISTS fkhr7vinhd1bb3ao02xp7xx135w;
ALTER TABLE sdn_id ADD CONSTRAINT fkhr7vinhd1bb3ao02xp7xx135w FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-26
ALTER TABLE sdn_address DROP CONSTRAINT IF EXISTS fkq3r589uqvw8tal0w3fg3ml3u3;
ALTER TABLE sdn_address ADD CONSTRAINT fkq3r589uqvw8tal0w3fg3ml3u3 FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-27
ALTER TABLE sdn_dateofbirth DROP CONSTRAINT IF EXISTS fkqpvdw1ojjiigbr3bdetll4spt;
ALTER TABLE sdn_dateofbirth ADD CONSTRAINT fkqpvdw1ojjiigbr3bdetll4spt FOREIGN KEY (sdnentry_id) REFERENCES sdn_entry (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-28
ALTER TABLE sdn_publishinfo DROP CONSTRAINT IF EXISTS fn_index_publishinfo_date;
ALTER TABLE sdn_publishinfo ADD CONSTRAINT fn_index_publishinfo_date UNIQUE (publishdate);

-- changeset bj:1635654854049-29
ALTER TABLE sdn_type DROP CONSTRAINT IF EXISTS uk_dsng0vlnme583bgcj9xc7jxr0;
ALTER TABLE sdn_type ADD CONSTRAINT uk_dsng0vlnme583bgcj9xc7jxr0 UNIQUE (name);

-- changeset bj:1635654854049-30
ALTER TABLE sdn_entry DROP CONSTRAINT IF EXISTS fkm5k876007qe9nwagtxesjcy2w;
ALTER TABLE sdn_entry ADD CONSTRAINT fkm5k876007qe9nwagtxesjcy2w FOREIGN KEY (vesselinfo_id) REFERENCES sdn_vesselinfo (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-31
ALTER TABLE sdn_entry DROP CONSTRAINT IF EXISTS fkqx56fxacbgbsdis0bj1oxgpuc;
ALTER TABLE sdn_entry ADD CONSTRAINT fkqx56fxacbgbsdis0bj1oxgpuc FOREIGN KEY (publishinfo_id) REFERENCES sdn_publishinfo (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- changeset bj:1635654854049-32
ALTER TABLE sdn_entry DROP CONSTRAINT IF EXISTS fkti9n6tr09vlp5scuygtrt3nd8;
ALTER TABLE sdn_entry ADD CONSTRAINT fkti9n6tr09vlp5scuygtrt3nd8 FOREIGN KEY (sdntype_id) REFERENCES sdn_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset bj:1635654854049-33
ALTER TABLE sdn_entry_program DROP CONSTRAINT IF EXISTS fkan7vfq1xrvuhaae5jo7w2d9ro;
ALTER TABLE sdn_entry_program ADD CONSTRAINT fkan7vfq1xrvuhaae5jo7w2d9ro FOREIGN KEY (program_id) REFERENCES sdn_program (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- changeset bj:1635654854049-34
CREATE SEQUENCE  IF NOT EXISTS hibernate_sequence AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-35
CREATE SEQUENCE  IF NOT EXISTS sdn_address_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-36
CREATE SEQUENCE  IF NOT EXISTS sdn_aka_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-37
CREATE SEQUENCE  IF NOT EXISTS sdn_citizenship_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-38
CREATE SEQUENCE  IF NOT EXISTS sdn_date_of_birth_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-39
CREATE SEQUENCE  IF NOT EXISTS sdn_entry_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-40
CREATE SEQUENCE  IF NOT EXISTS sdn_id_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-41
CREATE SEQUENCE  IF NOT EXISTS sdn_nationality_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-42
CREATE SEQUENCE  IF NOT EXISTS sdn_place_of_birth_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-43
CREATE SEQUENCE  IF NOT EXISTS sdn_program_generator AS bigint START WITH 1 INCREMENT BY 50 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-44
CREATE SEQUENCE  IF NOT EXISTS sdn_program_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-45
CREATE SEQUENCE  IF NOT EXISTS sdn_publishinfo_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-46
CREATE SEQUENCE  IF NOT EXISTS sdn_type_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-47
CREATE SEQUENCE  IF NOT EXISTS sdn_vessel_info_seq AS bigint START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- changeset bj:1635654854049-48
CREATE EXTENSION IF NOT EXISTS pg_trgm with schema pg_catalog;

-- changeset bj:1635654854049-49
alter table sdn_entry add column if not exists document_firstname_lastname tsvector;

-- changeset bj:1635654854049-50
alter table sdn_entry add column if not exists document_firstname tsvector;

-- changeset bj:1635654854049-51
alter table sdn_entry add column if not exists document_lastname tsvector;

-- changeset bj:1635654854049-52
create index if not exists document_firstname_lastname_idx on sdn_entry using GIN(document_firstname_lastname);

-- changeset bj:1635654854049-53
create index if not exists document_firstname_idx on sdn_entry using GIN(document_firstname);

-- changeset bj:1635654854049-54
create index if not exists document_lastname_idx on sdn_entry using GIN(document_lastname);


-- changeset bj:1635654854049-55 splitStatements:false
CREATE OR REPLACE FUNCTION sdn_entry_document_trigger_fun() returns trigger as $$
begin
	new.document_firstname_lastname:=to_tsvector(coalesce(new.firstname, '') || ' ' || coalesce(new.lastname, ''));
	new.document_firstname:=to_tsvector(coalesce(new.firstname, ''));
	new.document_lastname:=to_tsvector(coalesce(new.lastname, ''));
	return new;
end;
$$ language plpgsql;
drop trigger if exists sdn_entry_document_trigger on sdn_entry;
create trigger sdn_entry_document_trigger before insert or update on SDN_ENTRY for each row execute procedure sdn_entry_document_trigger_fun();

-- changeset bj:1635654854049-56
ALTER TABLE sdn_id ALTER COLUMN idnumber TYPE varchar(512);

package com.begui.ofac.search.sdn.resource;

import java.time.LocalDate;

import javax.ws.rs.core.Response.Status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.begui.ofac.db.sdn.PublishInfo;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;

@QuarkusTest
class PublishInfoResourceTest {

	@Test
	void testLatest() {
		PublishInfo publishInfo = RestAssured.given().when().get("/sdn/publishinfo/latest") //
				.then().statusCode(Status.OK.getStatusCode())
				.extract()
				.as(PublishInfo.class);
		Assertions.assertNotNull(publishInfo);
		Assertions.assertEquals(LocalDate.of(2021, 3, 1), publishInfo.publishDate);
		Assertions.assertEquals(2, publishInfo.id);
		Assertions.assertEquals(2, publishInfo.recordCount);

	}
}

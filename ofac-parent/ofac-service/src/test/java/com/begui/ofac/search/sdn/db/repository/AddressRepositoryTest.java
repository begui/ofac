package com.begui.ofac.search.sdn.db.repository;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class AddressRepositoryTest {

	@Inject
	AddressRepository repository;
	
	@ParameterizedTest
	@CsvSource({ "1,1", "2,2", "3,4", "4,5", "5,7" })
	void testGetByUid(int uid, int expected) {
		var addrOptional = repository.getByUid(uid);
		Assertions.assertTrue(addrOptional.isPresent());
		var addr = addrOptional.get();
		Assertions.assertEquals(expected, addr.id); 
	}
}

package com.begui.ofac.search.sdn.resource;

import javax.ws.rs.core.Response.Status;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.begui.ofac.db.sdn.Address;
import com.begui.ofac.db.sdn.SdnEntry;
import com.begui.ofac.db.sdn.SdnId;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;

@QuarkusTest
class EntryResourceTest {

	@ParameterizedTest
	@CsvSource({ "1,3", "2,2", "3,1" })
	void testGetAllByUid(String uid, String expected) {
		var sdnEntries = RestAssured.given().when().get("/sdn/entry/uid/all/" + uid) //
				.then().statusCode(Status.OK.getStatusCode()).extract().as(SdnEntry[].class);
		Assertions.assertNotNull(sdnEntries);
		Assertions.assertEquals(Integer.parseInt(expected), sdnEntries.length);

	}

	@Test
	void testGetAllByUid10() {
		RestAssured.given().when().get("/sdn/entry/uid/all/10") //
				.then().statusCode(Status.NO_CONTENT.getStatusCode());
	}

	@ParameterizedTest
	@CsvSource({ "1,sdntype1", "2,sdntype2" })
	void testGetLatestByUid(String uid, String expected) {
		RestAssured.given().when().get("/sdn/entry/uid/" + uid) //
				.then().statusCode(Status.OK.getStatusCode()) //
				.assertThat() //
				.body("publishInfo.publishDate", CoreMatchers.is("2021-03-01"))
				.body("sdnType.name", CoreMatchers.is(expected));

	}

	@Test
	void testGetLatestByUid3() {
		RestAssured.given().when().get("/sdn/entry/uid/" + 3) //
				.then().statusCode(Status.NO_CONTENT.getStatusCode());
	}

	@ParameterizedTest
	@CsvSource({ "1,2", "2,3" })
	void testGetAddressesByLatestSdnEntryUid(String uid, String expected) {
		var addresses = RestAssured.given().when().get("/sdn/entry/uid/" + uid + "/addresses") //
				.then().statusCode(Status.OK.getStatusCode()).extract().as(Address[].class);
		Assertions.assertNotNull(addresses);
		Assertions.assertEquals(Integer.parseInt(expected), addresses.length);

	}

	@Test
	void testGetAddressesByLatestSdnEntryUid10() {
		RestAssured.given().when().get("/sdn/entry/uid/10/addresses") //
				.then().statusCode(Status.NO_CONTENT.getStatusCode());
	}

	@ParameterizedTest
	@CsvSource({ "1,1", "2,2" })
	void testGetIdsByLatestSdnEntryUid(String uid, String expected) {
		var sdnIds = RestAssured.given().when().get("/sdn/entry/uid/" + uid + "/ids") //
				.then() //
				.statusCode(Status.OK.getStatusCode()) //
				.extract() //
				.as(SdnId[].class);
		Assertions.assertNotNull(sdnIds);
		Assertions.assertEquals(Integer.parseInt(expected), sdnIds.length);

	}

	@Test
	void testGetIdsByLatestSdnEntryUid10() {
		RestAssured.given() //
				.when().get("/sdn/entry/uid/10/ids") //
				.then() //
				.statusCode(Status.NO_CONTENT.getStatusCode());
	}

}

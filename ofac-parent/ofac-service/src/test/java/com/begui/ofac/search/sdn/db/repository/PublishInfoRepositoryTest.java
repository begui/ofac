package com.begui.ofac.search.sdn.db.repository;

import java.time.LocalDate;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class PublishInfoRepositoryTest {

	@Inject
	PublishInfoRepository publishInfoRepository;

	@DisplayName("Retrives the publish info by id")
	@ParameterizedTest(name = "{index} => id]{0}, count={1}, year={2}, month={3}, day={4}")
	@CsvSource({
		"1, 3, 2021, 1, 1",
		"2, 3, 2021, 3, 1",
		"3, 3, 2021, 2, 1"
	})
	void testPublishInfoId(long id, int count, int year, int month, int day) {
		Assertions.assertNotNull(publishInfoRepository);
		Assertions.assertEquals(count, publishInfoRepository.count());

		var publishInfo = publishInfoRepository.findById(id);
		Assertions.assertEquals(LocalDate.of(year, month, day), publishInfo.publishDate);
		Assertions.assertEquals(id, publishInfo.id);
		var sdnEntries = publishInfo.sdnEntry;
		Assertions.assertNotNull(sdnEntries);
		Assertions.assertEquals(publishInfo.recordCount, sdnEntries.size());
	}

	@Test
	void testGetLatestByPublishDate() {
		Assertions.assertNotNull(publishInfoRepository);
		Assertions.assertEquals(3, publishInfoRepository.count());

		var publishInfoOptional = publishInfoRepository.getLatestByPublishDate();
		Assertions.assertNotNull(publishInfoOptional);
		Assertions.assertEquals(true, publishInfoOptional.isPresent());
		var publishInfo = publishInfoOptional.get();
		Assertions.assertEquals(LocalDate.of(2021, 3, 1), publishInfo.publishDate);
		Assertions.assertEquals(2, publishInfo.id);
		Assertions.assertEquals(2, publishInfo.recordCount);
		var sdnEntries = publishInfo.sdnEntry;
		Assertions.assertNotNull(sdnEntries);
		Assertions.assertEquals(publishInfo.recordCount, sdnEntries.size());
	}

}

package com.begui.ofac.search.sdn.db.repository;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.begui.ofac.db.sdn.PublishInfo;
import com.begui.ofac.db.sdn.SdnEntry;
import com.begui.ofac.db.sdn.VesselInfo;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@Transactional
public class GeneralEntityTest {

	@Inject
	PublishInfoRepository publishInfoRepository;

	@Inject
	EntryRepository entryRepository;

	@Inject
	VesselInfoRepository vesselInfoRepository;

	@Test
	@AfterEach
	public void testVerifyAfterWeCleanedUpData() {
		Assertions.assertEquals(3, publishInfoRepository.count());
		Assertions.assertEquals(6, entryRepository.count());
		Assertions.assertEquals(0, vesselInfoRepository.count());

	}

	@Test
	public void testInsertAndDelete() {

		  var now = LocalDate.now();
		  
		  PublishInfo publishInfo = new PublishInfo(); publishInfo.publishDate = now;
		  publishInfo.recordCount = 1;
		  
		  SdnEntry sdnEntry = new SdnEntry(); 
		  sdnEntry.publishInfo = publishInfo;
		  sdnEntry.lastName = "testInsertAndDeleteLastName";
		  if(publishInfo.sdnEntry == null) {
			  publishInfo.sdnEntry = new ArrayList<>();
		  }
		  publishInfo.sdnEntry.add(sdnEntry); VesselInfo vesselInfo = new VesselInfo();
		  sdnEntry.vesselInfo = vesselInfo; vesselInfo.sdnEntry = sdnEntry;
		  
		  publishInfoRepository.persist(publishInfo);
		  
		  // We have items already loaded due to the import-test.sql
		  Assertions.assertEquals(4, publishInfoRepository.count() );
		  Assertions.assertEquals(7, entryRepository.count() );
		  Assertions.assertEquals(1, vesselInfoRepository.count() );
		  Assertions.assertEquals(sdnEntry, vesselInfo.sdnEntry);
		  Assertions.assertEquals(vesselInfo, sdnEntry.vesselInfo);
		  
		  // Lets test the cascade delete 
		  publishInfoRepository.delete(publishInfo);
		  
		 
	}

}

package com.begui.ofac.search.sdn.db.repository;

import javax.enterprise.context.ApplicationScoped;

import com.begui.ofac.db.sdn.SdnType;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class EntryTypeRepository implements PanacheRepository<SdnType> {

}

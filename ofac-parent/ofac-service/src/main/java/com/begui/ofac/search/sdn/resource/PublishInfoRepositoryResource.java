package com.begui.ofac.search.sdn.resource;


import javax.ws.rs.PathParam;

import com.begui.ofac.db.sdn.PublishInfo;
import com.begui.ofac.search.sdn.db.repository.PublishInfoRepository;

import io.quarkus.hibernate.orm.rest.data.panache.PanacheRepositoryResource;
import io.quarkus.rest.data.panache.MethodProperties;
import io.quarkus.rest.data.panache.ResourceProperties;

@ResourceProperties(hal = false, path = "sdn/publishinfo")
public interface PublishInfoRepositoryResource
		extends PanacheRepositoryResource<PublishInfoRepository, PublishInfo, Long> {

	@MethodProperties(exposed = false)
	public PublishInfo add(PublishInfo publishInfo);
	
	@MethodProperties(exposed = false)
	public PublishInfo update(@PathParam("id") Long id, PublishInfo publishInfo) ;
}

package com.begui.ofac.search.sdn.db.repository;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import com.begui.ofac.db.sdn.Address;
import com.begui.ofac.db.sdn.PublishInfo;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

@ApplicationScoped
public class AddressRepository implements PanacheRepository<Address> {

	public Optional<Address> getByUid(int uid) {
		return find("#Address.getLatestByUid", Parameters.with("uid", uid)).singleResultOptional();
	}

	public List<Address> getAddressBySdnEntryUid(int uid) {
		return find("#Address.getLatestAddressBySdnEntryUid", Parameters.with("uid", uid)).list();
	}

	public List<AddressDto> getLatestByPostalCodePage(String postalCode, int pageIndex, int pageSize) {
		return find("postalCode = :postalCode and sdnEntry.publishInfo.publishDate = (" + PublishInfo.LATESTPUBLISHED + ")", Parameters.with("postalCode", postalCode))
				.project(AddressDto.class) //
				.page(pageIndex, pageSize).list();
	}

	public List<AddressDto> getLatestByCountryPage(String country, int pageIndex, int pageSize) {
		return find("country = :country and sdnEntry.publishInfo.publishDate = (" + PublishInfo.LATESTPUBLISHED + ")", Parameters.with("country", country)) //
				.project(AddressDto.class) //
				.page(pageIndex, pageSize).list();
	}

}

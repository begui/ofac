package com.begui.ofac.search.sdn.db.repository;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import com.begui.ofac.db.sdn.SdnId;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

@ApplicationScoped
public class SdnIdRepository implements PanacheRepository<SdnId> {

	public Optional<SdnId> getByUid(int uid) {
		return find("#SdnId.getByUid", Parameters.with("uid", uid)).singleResultOptional();
	}

	public List<SdnId> getIdsBySdnEntryUid(int uid) {
		return find("#SdnId.getIdsBySdnEntryUid", Parameters.with("uid", uid)).list();
	}
}

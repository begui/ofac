package com.begui.ofac;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.begui.ofac.search.sdn.db.repository.PublishInfoRepository;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.jboss.logging.Logger;

@Liveness
@ApplicationScoped
public class OfacHealthCheck implements HealthCheck {
    private static Logger LOG = Logger.getLogger(OfacHealthCheck.class);

    @Inject
	PublishInfoRepository publishInfoRepository;

    @Override
    public HealthCheckResponse call() {

        var responseBuilder = HealthCheckResponse.named("OFac Sdn health check");
        try {
            publishInfoRepository.getLatestByPublishDate()//
            .ifPresentOrElse(entry->
                responseBuilder.withData("Ofac", "OK")
                .withData("Publish Date",entry.publishDate.toString() )
                .withData("Publish Count", entry.sdnEntry.size())
            , ()-> responseBuilder.withData("OFac", "No data found")
            );
        } catch (IllegalStateException e) {
            LOG.error("Error in health check", e);
            responseBuilder.down();
        }

        return responseBuilder.up().build();
    }

}

package com.begui.ofac.search.sdn.db.repository;

import java.time.LocalDate;

import io.quarkus.hibernate.orm.panache.common.ProjectedFieldName;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public record AddressDto(int uid, String address1, String address2, String address3, String city, String stateOrProvince, String postalCode, String country, @ProjectedFieldName("sdnEntry.uid") int sdnEntryUid, @ProjectedFieldName("sdnEntry.publishInfo.publishDate") LocalDate publishDate) {
	
}

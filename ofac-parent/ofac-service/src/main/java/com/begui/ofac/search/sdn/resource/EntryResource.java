package com.begui.ofac.search.sdn.resource;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.begui.ofac.db.dto.EntryFtsDto;
import com.begui.ofac.search.sdn.db.repository.AddressRepository;
import com.begui.ofac.search.sdn.db.repository.EntryRepository;
import com.begui.ofac.search.sdn.db.repository.EntryTypeRepository;
import com.begui.ofac.search.sdn.db.repository.SdnIdRepository;

@ApplicationScoped
@Path("sdn/entry")
public class EntryResource {

	@Inject
	EntryRepository entryRepository;

	@Inject
	AddressRepository addressRepository;

	@Inject
	SdnIdRepository sdnIdRepository;

	@Inject
	EntryTypeRepository entryTypeRepository;

	@Path("uid/all/{uid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllByUid(@PathParam("uid") Integer uid, //
			@QueryParam("page") @DefaultValue("0") int pageIndex, //
			@QueryParam("size") @DefaultValue("20") @Max(value = 50, message = "Max of 50 results at a time!") int pageSize) {
		var items = entryRepository.getAllByUid(uid, pageIndex, pageSize);
		if (items.isEmpty()) {
			return Response.status(Status.NO_CONTENT).build();
		}
		return Response.ok(items).build();
	}

	@Path("uid/{uid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLatestByUid(@PathParam("uid") Integer uid) {
		return entryRepository.getByUid(uid) //
				.map(entry -> Response.ok(entry).build()) //
				.orElse(Response.status(Status.NO_CONTENT).build());
	}

	@Path("uid/{uid}/addresses")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAddressesByLatestSdnEntryUid(@PathParam("uid") Integer uid) {
		var items = addressRepository.getAddressBySdnEntryUid(uid);
		if (items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	@Path("uid/{uid}/ids")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIdsByLatestSdnEntryUid(@PathParam("uid") Integer uid) {
		var items = sdnIdRepository.getIdsBySdnEntryUid(uid);
		if (items == null || items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	@Path("types")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllTypes() {
		var items = entryTypeRepository.findAll().list();
		if (items == null || items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	@Path("search/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response ftsFirstLastNameByPage(//
			@QueryParam("firstlastname") String firstlastname,  //
			@QueryParam("firstname") String firstname,  //
			@QueryParam("lastname") String lastname,  //
			@QueryParam("page") @DefaultValue("0") int pageIndex, //
			@QueryParam("size") @DefaultValue("20") @Max(value = 50, message = "Max of 50 results at a time!") int pageSize) {

				List<EntryFtsDto> items = null;
				if(firstlastname != null && firstlastname.length() > 0) {
					items = entryRepository.ftsFirstLastNameByPage(firstlastname, pageIndex, pageSize);
				}
				else if(firstname != null && firstname.length() > 0) {
					items = entryRepository.ftsFirstNameByPage(firstname, pageIndex, pageSize);
				}
				else if(lastname != null && lastname.length() > 0) {
					items = entryRepository.ftsLastNameByPage(lastname, pageIndex, pageSize);
				}

		if (items == null || items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}
}

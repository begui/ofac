package com.begui.ofac.search.sdn.resource;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.begui.ofac.db.sdn.Address;
import com.begui.ofac.search.sdn.db.repository.AddressDto;
import com.begui.ofac.search.sdn.db.repository.AddressRepository;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

@ApplicationScoped
@Path("sdn/address")
public class AddressResource {

	@Inject
	AddressRepository addressRepository;

	@Operation(summary = "Returns the latest Address by Uid")
	@APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = Address.class)))
	@APIResponse(responseCode = "404", description = "The Address is not found")
	@Path("uid/{uid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByUid(@PathParam("uid") Integer uid) {
		return addressRepository.getByUid(uid) //
				.map(entry -> Response.ok(entry).build()) //
				.orElse(Response.status(Status.NOT_FOUND).build());
	}

	@Operation(summary = "Returns the list of Addresses By Postal Code")
	@APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = AddressDto.class)))
	@APIResponse(responseCode = "404", description = "The Address is not found")
	@Path("postal/{postalcode}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByPostalCode( //
			@PathParam("postalcode") String postalCode, //
			@QueryParam("page") @DefaultValue("0") int pageIndex,
			@QueryParam("size") @DefaultValue("20") @Max(value = 50, message = "Max of 50 results at a time!") int pageSize) {
		var items = addressRepository.getLatestByPostalCodePage(postalCode, pageIndex, pageSize);
		if (items.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(items).build();
	}
	
	@Operation(summary = "Returns the list of Addresses By Country")
	@APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = AddressDto.class)))
	@APIResponse(responseCode = "404", description = "The Address is not found")
	@Path("country/{country}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByCountry( //
			@PathParam("country") String country, //
			@QueryParam("page") @DefaultValue("0") int pageIndex,
			@QueryParam("size") @DefaultValue("20") @Max(value = 50, message = "Max of 50 results at a time!") int pageSize) {
		var items = addressRepository.getLatestByCountryPage(country, pageIndex, pageSize);
		if (items.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(items).build();
	}

}

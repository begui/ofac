package com.begui.ofac.search.sdn.db.repository;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import com.begui.ofac.db.sdn.PublishInfo;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class PublishInfoRepository implements PanacheRepository<PublishInfo> {

	public Optional<PublishInfo> getLatestByPublishDate() {
		return find("#PublishInfo.getLatestByPublishDate").singleResultOptional();
	}

}

package com.begui.ofac.search.sdn.db.repository;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;

import com.begui.ofac.db.dto.EntryFtsDto;
import com.begui.ofac.db.sdn.SdnEntry;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

@ApplicationScoped
public class EntryRepository implements PanacheRepository<SdnEntry> {

	public List<SdnEntry> getAllByUid(int uid, int pageIndex, int pageSize) {
		return find("#SdnEntry.getAllByUid", Parameters.with("uid", uid)) //
				.page(pageIndex, pageSize) //
				.list();
	}

	public Optional<SdnEntry> getByUid(int uid) {
		return find("#SdnEntry.getByUid", Parameters.with("uid", uid)).singleResultOptional();
	}

	public List<EntryFtsDto> ftsFirstLastNameByPage(String name, int pageIndex, int pageSize) {
		return this.getEntityManager().createNamedQuery("SdnEntry.ftsFirstLastNameByPage", EntryFtsDto.class)
				.setParameter(1, name) //
				.setParameter(2, pageIndex) //
				.setParameter(3, pageSize) //
				.getResultList();
	}

	public List<EntryFtsDto> ftsFirstNameByPage(String firstname, int pageIndex, int pageSize) {
		return this.getEntityManager().createNamedQuery("SdnEntry.ftsFirstNameByPage", EntryFtsDto.class)
				.setParameter(1, firstname) //
				.setParameter(2, pageIndex) //
				.setParameter(3, pageSize) //
				.getResultList();

	}

	public List<EntryFtsDto> ftsLastNameByPage(String lastname, int pageIndex, int pageSize) {
		return this.getEntityManager().createNamedQuery("SdnEntry.ftsLastNameByPage", EntryFtsDto.class)
				.setParameter(1, lastname) //
				.setParameter(2, pageIndex) //
				.setParameter(3, pageSize) //
				.getResultList();

	}

}

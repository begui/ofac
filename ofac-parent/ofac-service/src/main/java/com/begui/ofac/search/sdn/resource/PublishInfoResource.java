package com.begui.ofac.search.sdn.resource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.begui.ofac.db.sdn.PublishInfo;
import com.begui.ofac.search.sdn.db.repository.PublishInfoRepository;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

@ApplicationScoped
@Path("sdn/publishinfo")
public class PublishInfoResource {

	@Inject
	PublishInfoRepository publishInfoRepository;

	@Operation(summary = "Returns the latest published sdn info.")
	@APIResponse(responseCode = "200", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = PublishInfo.class)))
	@APIResponse(responseCode = "404", description = "The SDN Publish Info is not found")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("latest")
	public Response getLatest() {
		return publishInfoRepository.getLatestByPublishDate() //
				.map(entry -> Response.ok(entry).build()) //
				.orElse(Response.status(Status.NOT_FOUND).build());
	}
}

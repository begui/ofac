insert into SDN_PublishInfo  (id, publishdate, recordcount) values ( 1, '2021-01-01', 1 );
insert into SDN_PublishInfo  (id, publishdate, recordcount) values ( 2, '2021-03-01', 2 );
insert into SDN_PublishInfo  (id, publishdate, recordcount) values ( 3, '2021-02-01', 3 );

insert into SDN_Type (id, name) values (1, 'sdntype1');
insert into SDN_Type (id, name) values (2, 'sdntype2');
insert into SDN_Type (id, name) values (3, 'sdntype3');

-- 1
insert into SDN_Entry (id, firstname, lastname, remarks, sdntype_id, title, uid, publishinfo_id, vesselinfo_id) values (1, 'firstname1', 'lastname1', 'remarks1', 1, 'title1', 1, 1, null );
-- 3
insert into SDN_Entry (id, firstname, lastname, remarks, sdntype_id, title, uid, publishinfo_id, vesselinfo_id) values (2, 'firstname1', 'lastname1', 'remarks1', 1, 'title1', 1, 2, null );
insert into SDN_Entry (id, firstname, lastname, remarks, sdntype_id, title, uid, publishinfo_id, vesselinfo_id) values (4, 'firstname2', 'lastname2', 'remarks2', 2, 'title2', 2, 2, null );
-- 2
insert into SDN_Entry (id, firstname, lastname, remarks, sdntype_id, title, uid, publishinfo_id, vesselinfo_id) values (3, 'firstname1', 'lastname1', 'remarks1', 1, 'title1', 1, 3, null );
insert into SDN_Entry (id, firstname, lastname, remarks, sdntype_id, title, uid, publishinfo_id, vesselinfo_id) values (5, 'firstname2', 'lastname2', 'remarks2', 2, 'title2', 2, 3, null );
insert into SDN_Entry (id, firstname, lastname, remarks, sdntype_id, title, uid, publishinfo_id, vesselinfo_id) values (6, 'firstname3', 'lastname3', 'remarks3', 3, 'title3', 3, 3, null );

insert into SDN_Address(id, address1, address2, address3, city, country, postalcode, stateorprovince, uid, sdnentry_id) values(1, 'addr1-1', 'addr2-1', 'addr3-1', 'city-1', 'country-1', 'postalcode-1', 'stateorprovince-1', 1, 4);
insert into SDN_Address(id, address1, address2, address3, city, country, postalcode, stateorprovince, uid, sdnentry_id) values(2, 'addr1-1', 'addr2-1', 'addr3-1', 'city-1', 'country-1', 'postalcode-1', 'stateorprovince-1', 2, 2);

insert into SDN_Address(id, address1, address2, address3, city, country, postalcode, stateorprovince, uid, sdnentry_id) values(4, 'addr1-2', 'addr2-2', 'addr3-2', 'city-2', 'country-2', 'postalcode-2', 'stateorprovince-2', 3, 4);
insert into SDN_Address(id, address1, address2, address3, city, country, postalcode, stateorprovince, uid, sdnentry_id) values(5, 'addr1-2', 'addr2-2', 'addr3-2', 'city-2', 'country-2', 'postalcode-2', 'stateorprovince-2', 4, 2);

insert into SDN_Address(id, address1, address2, address3, city, country, postalcode, stateorprovince, uid, sdnentry_id) values(7, 'addr1-3', 'addr2-3', 'addr3-3', 'city-3', 'country-3', 'postalcode-3', 'stateorprovince-1', 5, 4);

insert into SDN_Id (id, expirationdate, idcountry, idnumber, idtype, issuedate, uid, sdnentry_id) values (1, null, 'country-1', 'idnumber-1', 'idtype-1', null, 97, 2 );
insert into SDN_ID (id, expirationdate, idcountry, idnumber, idtype, issuedate, uid, sdnentry_id) values (2, null, 'country-2', 'idnumber-2', 'idtype-2', null, 98, 4 );
insert into SDN_ID (id, expirationdate, idcountry, idnumber, idtype, issuedate, uid, sdnentry_id) values (3, null, 'country-3', 'idnumber-3', 'idtype-3', null, 99, 4 );

alter sequence sdn_publishinfo_seq restart with 4;
alter sequence sdn_type_seq restart with 4;
alter sequence sdn_entry_seq restart with 7;
alter sequence sdn_address_seq restart with 8;
alter sequence sdn_id_seq restart with 4;
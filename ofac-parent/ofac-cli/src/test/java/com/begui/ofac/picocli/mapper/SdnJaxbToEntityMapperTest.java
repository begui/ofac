package com.begui.ofac.picocli.mapper;

import java.time.LocalDate;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.begui.ofac.sdn.SdnList;
import com.begui.ofac.sdn.SdnList.PublshInformation;
import com.begui.ofac.sdn.SdnList.SdnEntry.ProgramList;
import com.github.javafaker.Faker;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@ApplicationScoped
class SdnJaxbToEntityMapperTest {

	@Inject
	SdnJaxbToEntityMapper mapper;

	@Test
	void jaxbToEntityTest() {
		
		Faker faker = new Faker();

		SdnList sdnList = new SdnList();
		sdnList.setPublshInformation(new PublshInformation());
		sdnList.getPublshInformation().setPublishDate("01/01/2021");
		sdnList.getPublshInformation().setRecordCount(1);

		var entrys = sdnList.getSdnEntry();
		var entry = new SdnList.SdnEntry();

		entry.setAddressList(new SdnList.SdnEntry.AddressList());
		var addr = new SdnList.SdnEntry.AddressList.Address();
		addr.setAddress1(faker.address().streetAddress());
		addr.setAddress2(faker.address().streetAddress());
		addr.setAddress3(faker.address().streetAddress());
		addr.setCity(faker.address().city());
		addr.setCountry(faker.address().country());
		addr.setPostalCode(faker.address().zipCode());
		addr.setStateOrProvince(faker.address().state());
		addr.setUid(faker.number().randomDigit());
		entry.getAddressList().getAddress().add(addr);
		
		
		entry.setAkaList(new SdnList.SdnEntry.AkaList());
		var aka = new SdnList.SdnEntry.AkaList.Aka();
		aka.setCategory(faker.lorem().fixedString(5));
		aka.setType(faker.lorem().fixedString(5));
		aka.setUid(faker.number().randomDigit());
		aka.setFirstName(faker.name().firstName());
		aka.setLastName(faker.name().lastName());
		entry.getAkaList().getAka().add(aka);
		
		entry.setCitizenshipList(new SdnList.SdnEntry.CitizenshipList() );
		var cit = new SdnList.SdnEntry.CitizenshipList.Citizenship();
		cit.setCountry(faker.address().country());
		cit.setMainEntry(true);
		cit.setUid(faker.number().randomDigit());
		
		entry.getCitizenshipList().getCitizenship().add(cit);
		
		entry.setDateOfBirthList(new SdnList.SdnEntry.DateOfBirthList() );
		var dobItem = new SdnList.SdnEntry.DateOfBirthList.DateOfBirthItem();
		dobItem.setDateOfBirth("01/01/2021");
		dobItem.setMainEntry(true);
		dobItem.setUid(faker.number().randomDigit());
		entry.getDateOfBirthList().getDateOfBirthItem().add(dobItem);


		entry.setFirstName(faker.name().firstName());
		entry.setLastName(faker.name().lastName());

		entry.setIdList(new SdnList.SdnEntry.IdList() );
		var id = new SdnList.SdnEntry.IdList.Id();
		id.setExpirationDate(faker.lorem().fixedString(5));
		id.setIdCountry(faker.lorem().fixedString(5));
		id.setIdNumber(faker.lorem().fixedString(5));
		id.setIdType(faker.lorem().fixedString(5));
		id.setIssueDate("01/01/2020");
		id.setUid(faker.number().randomDigit());
		entry.getIdList().getId().add(id);

		entry.setNationalityList(new SdnList.SdnEntry.NationalityList() );
		var nat = new SdnList.SdnEntry.NationalityList.Nationality();
		nat.setCountry(faker.address().country());
		nat.setMainEntry(true);
		nat.setUid(faker.number().randomDigit());
		entry.getNationalityList().getNationality().add(nat);
		
		entry.setPlaceOfBirthList(new SdnList.SdnEntry.PlaceOfBirthList());
		var pofi = new SdnList.SdnEntry.PlaceOfBirthList.PlaceOfBirthItem();
		pofi.setMainEntry(true);
		pofi.setPlaceOfBirth(faker.address().country());
		pofi.setUid(faker.number().randomDigit());
		
		entry.getPlaceOfBirthList().getPlaceOfBirthItem().add(pofi);
		
		entry.setProgramList(new ProgramList());
		entry.getProgramList().getProgram().add(faker.lorem().fixedString(5));
		entry.getProgramList().getProgram().add(faker.lorem().fixedString(5));

		entry.setRemarks(faker.lorem().fixedString(5));
		entry.setSdnType(faker.lorem().fixedString(5));
		entry.setTitle(faker.lorem().fixedString(5));
		entry.setUid(faker.number().randomDigit());

		var vesselInfo = new SdnList.SdnEntry.VesselInfo();
		vesselInfo.setCallSign(faker.lorem().fixedString(5));
		vesselInfo.setGrossRegisteredTonnage(faker.number().randomDigit());
		vesselInfo.setTonnage(faker.number().randomDigit());
		vesselInfo.setVesselFlag(faker.lorem().fixedString(5));
		vesselInfo.setVesselOwner(faker.lorem().fixedString(5));
		vesselInfo.setVesselType(faker.lorem().fixedString(5));
		entry.setVesselInfo(vesselInfo);
		entrys.add(entry);

		{
			var publishInfo = mapper.toPublishInfo(sdnList);
			Assertions.assertEquals(sdnList.getPublshInformation().getRecordCount(), publishInfo.recordCount);
			Assertions.assertEquals(LocalDate.parse("2021-01-01"), publishInfo.publishDate);

			Assertions.assertEquals(1, publishInfo.sdnEntry.size() );
			var sdnEntry = publishInfo.sdnEntry.get(0);
			Assertions.assertNotNull(publishInfo);
			
			Assertions.assertEquals(1, sdnEntry.addressList.size());
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getUid(), sdnEntry.addressList.get(0).uid);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).address1);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getAddress1(), sdnEntry.addressList.get(0).address1);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).address2);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getAddress2(), sdnEntry.addressList.get(0).address2);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).address3);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getAddress3(), sdnEntry.addressList.get(0).address3);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).city);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getCity(), sdnEntry.addressList.get(0).city);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).country);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getCountry(), sdnEntry.addressList.get(0).country);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).postalCode);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getPostalCode(), sdnEntry.addressList.get(0).postalCode);
			Assertions.assertNotNull(sdnEntry.addressList.get(0).stateOrProvince);
			Assertions.assertEquals(entry.getAddressList().getAddress().get(0).getStateOrProvince(), sdnEntry.addressList.get(0).stateOrProvince);
			
			Assertions.assertEquals(1, sdnEntry.akaList.size());
			Assertions.assertEquals(entry.getAkaList().getAka().get(0).getCategory(), sdnEntry.akaList.get(0).category);
			Assertions.assertEquals(entry.getAkaList().getAka().get(0).getUid(), sdnEntry.akaList.get(0).uid);
			Assertions.assertEquals(entry.getAkaList().getAka().get(0).getFirstName(), sdnEntry.akaList.get(0).firstName);
			Assertions.assertEquals(entry.getAkaList().getAka().get(0).getLastName(), sdnEntry.akaList.get(0).lastName);
			Assertions.assertEquals(entry.getAkaList().getAka().get(0).getType(), sdnEntry.akaList.get(0).type);
			Assertions.assertNotNull(sdnEntry.akaList.get(0).sdnEntry);
			
			Assertions.assertEquals(1, sdnEntry.citizenshipList.size());
			Assertions.assertNotNull(sdnEntry.citizenshipList.get(0).country);
			Assertions.assertEquals(entry.getCitizenshipList().getCitizenship().get(0).getCountry(), sdnEntry.citizenshipList.get(0).country);
			Assertions.assertEquals(entry.getCitizenshipList().getCitizenship().get(0).isMainEntry(), sdnEntry.citizenshipList.get(0).mainEntry);
			Assertions.assertEquals(entry.getCitizenshipList().getCitizenship().get(0).getUid(), sdnEntry.citizenshipList.get(0).uid);
			Assertions.assertNotNull(sdnEntry.citizenshipList.get(0).sdnEntry);

			Assertions.assertEquals(1, sdnEntry.dateOfBirthList.size());
			Assertions.assertNotNull(sdnEntry.dateOfBirthList.get(0).dateOfBirth);
			Assertions.assertEquals(entry.getDateOfBirthList().getDateOfBirthItem().get(0).getDateOfBirth(), sdnEntry.dateOfBirthList.get(0).dateOfBirth);
			Assertions.assertEquals(entry.getDateOfBirthList().getDateOfBirthItem().get(0).isMainEntry(), sdnEntry.dateOfBirthList.get(0).mainEntry);
			Assertions.assertEquals(entry.getDateOfBirthList().getDateOfBirthItem().get(0).getUid(), sdnEntry.dateOfBirthList.get(0).uid);
			Assertions.assertNotNull(sdnEntry.dateOfBirthList.get(0).sdnEntry);

			
			Assertions.assertNotNull(sdnEntry.firstName);
			Assertions.assertEquals( entry.getFirstName(), sdnEntry.firstName);
			Assertions.assertNotNull(sdnEntry.lastName);
			Assertions.assertEquals( entry.getLastName(), sdnEntry.lastName);
			
			Assertions.assertEquals(1, sdnEntry.idList.size());
			Assertions.assertNotNull(sdnEntry.idList.get(0).expirationDate);
			Assertions.assertEquals(entry.getIdList().getId().get(0).getExpirationDate(), sdnEntry.idList.get(0).expirationDate);
			Assertions.assertNotNull(sdnEntry.idList.get(0).idCountry);
			Assertions.assertEquals(entry.getIdList().getId().get(0).getIdCountry(), sdnEntry.idList.get(0).idCountry);
			Assertions.assertNotNull(sdnEntry.idList.get(0).idNumber);
			Assertions.assertEquals(entry.getIdList().getId().get(0).getIdNumber(), sdnEntry.idList.get(0).idNumber);
			Assertions.assertNotNull(sdnEntry.idList.get(0).idType);
			Assertions.assertEquals(entry.getIdList().getId().get(0).getIdType(), sdnEntry.idList.get(0).idType);
			Assertions.assertNotNull(sdnEntry.idList.get(0).issueDate);
			Assertions.assertEquals(entry.getIdList().getId().get(0).getIssueDate(), sdnEntry.idList.get(0).issueDate);
			Assertions.assertEquals(entry.getIdList().getId().get(0).getUid(), sdnEntry.idList.get(0).uid);
			Assertions.assertNotNull(sdnEntry.idList.get(0).sdnEntry);
			
			Assertions.assertEquals(1, sdnEntry.nationalityList.size());
			Assertions.assertEquals(entry.getNationalityList().getNationality().get(0).isMainEntry(), sdnEntry.nationalityList.get(0).mainEntry);
			Assertions.assertEquals(entry.getNationalityList().getNationality().get(0).getUid(), sdnEntry.nationalityList.get(0).uid);
			Assertions.assertNotNull( sdnEntry.nationalityList.get(0).country);
			Assertions.assertEquals(entry.getNationalityList().getNationality().get(0).getCountry(), sdnEntry.nationalityList.get(0).country);
			Assertions.assertNotNull(sdnEntry.nationalityList.get(0).sdnEntry);

			
			Assertions.assertEquals(1, sdnEntry.placeOfBirthList.size());
			Assertions.assertEquals(entry.getPlaceOfBirthList().getPlaceOfBirthItem().get(0).isMainEntry(), sdnEntry.placeOfBirthList.get(0).mainEntry);
			Assertions.assertEquals(entry.getPlaceOfBirthList().getPlaceOfBirthItem().get(0).getUid(), sdnEntry.placeOfBirthList.get(0).uid);
			Assertions.assertNotNull( sdnEntry.placeOfBirthList.get(0).placeOfBirth);
			Assertions.assertEquals(entry.getPlaceOfBirthList().getPlaceOfBirthItem().get(0).getPlaceOfBirth(), sdnEntry.placeOfBirthList.get(0).placeOfBirth);
			Assertions.assertNotNull(sdnEntry.placeOfBirthList.get(0).sdnEntry);
			
			Assertions.assertNotNull( sdnEntry.programs);
			Assertions.assertEquals(2, sdnEntry.programs.size() );

			Assertions.assertEquals(entry.getRemarks(), sdnEntry.remarks);
			Assertions.assertEquals(entry.getSdnType(), sdnEntry.sdnType.name);
			Assertions.assertEquals(entry.getTitle(), sdnEntry.title);
			Assertions.assertEquals(entry.getUid(), sdnEntry.uid );

			Assertions.assertNotNull(sdnEntry.vesselInfo);
			Assertions.assertNotNull( sdnEntry.vesselInfo.callSign);
			Assertions.assertEquals(vesselInfo.getCallSign(), sdnEntry.vesselInfo.callSign);
			Assertions.assertNotNull( sdnEntry.vesselInfo.grossRegisteredTonnage);
			Assertions.assertEquals(vesselInfo.getGrossRegisteredTonnage(), sdnEntry.vesselInfo.grossRegisteredTonnage);
			Assertions.assertNotNull( sdnEntry.vesselInfo.tonnage);
			Assertions.assertEquals(vesselInfo.getTonnage(), sdnEntry.vesselInfo.tonnage);
			Assertions.assertNotNull( sdnEntry.vesselInfo.vesselFlag);
			Assertions.assertEquals(vesselInfo.getVesselFlag(), sdnEntry.vesselInfo.vesselFlag);
			Assertions.assertNotNull( sdnEntry.vesselInfo.vesselOwner);
			Assertions.assertEquals(vesselInfo.getVesselOwner(), sdnEntry.vesselInfo.vesselOwner);
			Assertions.assertNotNull( sdnEntry.vesselInfo.vesselType);
			Assertions.assertEquals(vesselInfo.getVesselType(), sdnEntry.vesselInfo.vesselType);
			Assertions.assertNotNull(sdnEntry.vesselInfo.sdnEntry );
		}

	}

}

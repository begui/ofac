package com.begui.ofac.picocli;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import io.quarkus.liquibase.LiquibaseFactory;
import liquibase.Liquibase;
import liquibase.changelog.ChangeSetStatus;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "ofacDbSync", description = {"Runs the changeset"},  mixinStandardHelpOptions = true)
public class OFacDBCommand implements Runnable {

	public static final Logger LOG = Logger.getLogger(OFacDBCommand.class);

	@Inject
	OFacLiquibaseService liquibaseService;

	@CommandLine.Option(names = { "-d", "Drops the entire Schema" }, description = "WARN: Drops the entire schema. Are you sure (Y/y) ????", required = false, interactive = true)
	char dropAllTables;

	@Override
	public void run() {
		LOG.info("Running ofacDbSync ");
		liquibaseService.update(dropAllTables == 'Y' || dropAllTables == 'y');
	}
}

@ApplicationScoped
class OFacLiquibaseService {
	public static final Logger LOG = Logger.getLogger(OFacLiquibaseService.class);
	@Inject
	LiquibaseFactory liquibaseFactory;

	public void update(boolean dropall) {
		try (Liquibase liquibase = liquibaseFactory.createLiquibase()) {
			if (dropall) {
				LOG.info("!!!!Dropping the schema!!!!");
				liquibase.dropAll();
			}
			liquibase.validate();
			liquibase.update(liquibaseFactory.createContexts(), liquibaseFactory.createLabels());
			// Get the list of liquibase change set statuses
			List<ChangeSetStatus> status = liquibase.getChangeSetStatuses(liquibaseFactory.createContexts(),
					liquibaseFactory.createLabels());
			for (var s : status) {
				LOG.info(s);
			}
		} catch (Exception e) {
			LOG.error("Failed to execute ", e);
		}
	}
}

package com.begui.ofac.picocli.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.begui.ofac.db.sdn.Address;
import com.begui.ofac.db.sdn.Aka;
import com.begui.ofac.db.sdn.Citizenship;
import com.begui.ofac.db.sdn.DateOfBirth;
import com.begui.ofac.db.sdn.Nationality;
import com.begui.ofac.db.sdn.PlaceOfBirth;
import com.begui.ofac.db.sdn.Program;
import com.begui.ofac.db.sdn.PublishInfo;
import com.begui.ofac.db.sdn.SdnEntry;
import com.begui.ofac.db.sdn.SdnId;
import com.begui.ofac.db.sdn.SdnType;
import com.begui.ofac.db.sdn.VesselInfo;

@Mapper(componentModel = "cdi")
public interface SdnJaxbToEntityMapper {

	@Mapping(target = "publishDate", source = "publshInformation.publishDate", dateFormat = "MM/dd/yyyy")
	@Mapping(target = "recordCount", source = "publshInformation.recordCount")
	@Mapping(target = "id", ignore = true)
	public PublishInfo toPublishInfo(com.begui.ofac.sdn.SdnList sdnList);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "publishInfo", ignore = true)
	@Mapping(target = "programs", source = "programList.program", qualifiedByName = "programsMapping")
	@Mapping(target = "sdnType", source ="sdnType", qualifiedByName = "sdnTypeMapping")
	@Mapping(target = "idList", source = "idList.id")
	@Mapping(target = "akaList", source = "akaList.aka")
	@Mapping(target = "addressList", source = "addressList.address")
	@Mapping(target = "nationalityList", source = "nationalityList.nationality")
	@Mapping(target = "citizenshipList", source = "citizenshipList.citizenship")
	@Mapping(target = "dateOfBirthList", source = "dateOfBirthList.dateOfBirthItem")
	@Mapping(target = "placeOfBirthList", source = "placeOfBirthList.placeOfBirthItem")
	public SdnEntry toSdnEntry(com.begui.ofac.sdn.SdnList.SdnEntry entry);

	public List<SdnEntry> toSdnEntryList(List<com.begui.ofac.sdn.SdnList.SdnEntry> sdnEntry);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public VesselInfo toVesselInfo(com.begui.ofac.sdn.SdnList.SdnEntry.VesselInfo item);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public SdnId toSdnId(com.begui.ofac.sdn.SdnList.SdnEntry.IdList.Id item);

	public List<SdnId> toSdnIdList(List<com.begui.ofac.sdn.SdnList.SdnEntry.IdList.Id> list);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public Aka toAka(com.begui.ofac.sdn.SdnList.SdnEntry.AkaList.Aka item);

	public List<Aka> toAkaList(List<com.begui.ofac.sdn.SdnList.SdnEntry.AkaList.Aka> list);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public Address toAddress(com.begui.ofac.sdn.SdnList.SdnEntry.AddressList.Address item);

	public List<Address> toAddressList(List<com.begui.ofac.sdn.SdnList.SdnEntry.AddressList.Address> list);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public Nationality toNationality(com.begui.ofac.sdn.SdnList.SdnEntry.NationalityList.Nationality item);

	public List<Nationality> toNationalityList(List<com.begui.ofac.sdn.SdnList.SdnEntry.NationalityList.Nationality> list);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public Citizenship toCitizenship(com.begui.ofac.sdn.SdnList.SdnEntry.CitizenshipList.Citizenship list);

	public List<Citizenship> toCitizenshipList(
			List<com.begui.ofac.sdn.SdnList.SdnEntry.CitizenshipList.Citizenship> item);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public DateOfBirth toDateOfBirth(com.begui.ofac.sdn.SdnList.SdnEntry.DateOfBirthList.DateOfBirthItem item);

	public List<DateOfBirth> toDateOfBirthList(
			List<com.begui.ofac.sdn.SdnList.SdnEntry.DateOfBirthList.DateOfBirthItem> list);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "sdnEntry", ignore = true)
	public PlaceOfBirth toPlaceOfBirth(com.begui.ofac.sdn.SdnList.SdnEntry.PlaceOfBirthList.PlaceOfBirthItem item);

	public List<PlaceOfBirth> toPlaceOfBirthList(
			List<com.begui.ofac.sdn.SdnList.SdnEntry.PlaceOfBirthList.PlaceOfBirthItem> list);
	
	@Named("programsMapping")
	default Program programStrToProgram(String name) {
		var p = new Program();
		p.name = name;
		return p;
	}
	
	@Named("sdnTypeMapping")
	default SdnType sdnTypeStrToSdnType(String name) {
		var s = new SdnType();
		s.name = name;
		return s;
	}

}

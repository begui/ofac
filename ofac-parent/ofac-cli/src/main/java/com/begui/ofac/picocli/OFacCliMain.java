package com.begui.ofac.picocli;

import io.quarkus.picocli.runtime.annotations.TopCommand;
import picocli.CommandLine;

/*
@QuarkusMain
@CommandLine.Command(name= "ofac-cli", mixinStandardHelpOptions = true, subcommands = { OFacSdnCommand.class, OFacSdnAdvCommand.class, OFacDBCommand.class })
public class OFacCliMain implements Runnable, QuarkusApplication {
	@Inject
    CommandLine.IFactory factory; 

    @Override
    public void run() {
        // business logic
    }

    @Override
    public int run(String... args) throws Exception {
        return new CommandLine(this, factory).execute(args);
    }
}
*/

@TopCommand
@CommandLine.Command(mixinStandardHelpOptions = true, subcommands = { OFacSdnCommand.class, OFacSdnAdvCommand.class,
		OFacDBCommand.class })
public class OFacCliMain {
	// Note: Adding a quarkus main break the camel code
	// https://camel.zulipchat.com/#narrow/stream/257302-camel-quarkus/topic/Disabling.20camel.20main
	//https://camel.apache.org/camel-quarkus/2.7.x/reference/extensions/core.html#quarkus.camel.main.arguments.on-unknown
}
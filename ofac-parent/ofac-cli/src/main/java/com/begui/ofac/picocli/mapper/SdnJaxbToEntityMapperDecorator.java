package com.begui.ofac.picocli.mapper;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.begui.ofac.db.sdn.Address;
import com.begui.ofac.db.sdn.Aka;
import com.begui.ofac.db.sdn.Citizenship;
import com.begui.ofac.db.sdn.DateOfBirth;
import com.begui.ofac.db.sdn.Nationality;
import com.begui.ofac.db.sdn.PlaceOfBirth;
import com.begui.ofac.db.sdn.PublishInfo;
import com.begui.ofac.db.sdn.SdnEntry;
import com.begui.ofac.sdn.SdnList;

@Dependent
@Decorator
public abstract class SdnJaxbToEntityMapperDecorator implements SdnJaxbToEntityMapper {

	@Inject
	@Delegate
	SdnJaxbToEntityMapper delegate;

	@Override
	public PublishInfo toPublishInfo(SdnList sdnList) {

		var publishInfo = delegate.toPublishInfo(sdnList);
		publishInfo.sdnEntry.stream().forEach(sdnEntry -> {
			sdnEntry.publishInfo = publishInfo;
//			setSdnEntryOnObjects(sdnEntry);
		});
		return publishInfo;
	}

	@Transactional
	@Override
	public SdnEntry toSdnEntry(com.begui.ofac.sdn.SdnList.SdnEntry entry) {
		var sdnEntry = delegate.toSdnEntry(entry);
		//Note: I tried to inject the entity manager here to create the Program entities 
		// 1. it failed
		// 2. decided it would be a bad idea since it would tie the db to this. 
		setSdnEntryOnObjects(sdnEntry);
		return sdnEntry;
	}

	/**
	 * @param entry
	 */
	private void setSdnEntryOnObjects(SdnEntry entry) {
		if (entry.vesselInfo != null) {
			entry.vesselInfo.sdnEntry = entry;
		}
		if (entry.addressList != null) {
			for (Address addr : entry.addressList) {
				addr.sdnEntry = entry;
			}
		}
		if (entry.akaList != null) {
			for (Aka aka : entry.akaList) {
				aka.sdnEntry = entry;
			}
		}
		if (entry.idList != null) {
			for (var id : entry.idList) {
				id.sdnEntry = entry;
			}
		}
		if (entry.citizenshipList != null) {
			for (Citizenship citz : entry.citizenshipList) {
				citz.sdnEntry = entry;
			}
		}
		if (entry.placeOfBirthList != null) {
			for (PlaceOfBirth pob : entry.placeOfBirthList) {
				pob.sdnEntry = entry;
			}
		}
		if (entry.dateOfBirthList != null) {
			for (DateOfBirth dob : entry.dateOfBirthList) {
				dob.sdnEntry = entry;
			}
		}
		if (entry.nationalityList != null) {
			for (Nationality nat : entry.nationalityList) {
				nat.sdnEntry = entry;
			}
		}
	}
}

package com.begui.ofac.picocli;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.zipfile.ZipSplitter;
import org.jboss.logging.Logger;

import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "ofacSdnAdv", mixinStandardHelpOptions = true)
public class OFacSdnAdvCommand implements Runnable {
	public static final Logger LOG = Logger.getLogger(OFacSdnAdvCommand.class);

	@Inject
	ProducerTemplate producerTemplate;

	@Inject
	OFacSdnAdvancedRoutes routes;

	@Inject
	CommandLine.IFactory factory;

	enum ExecuteType {
		fetch, populate, fetchAndPopulate
	};

	@CommandLine.Option(names = { "-e" }, description = "Possible Values: ${COMPLETION-CANDIDATES}", required = true)
	ExecuteType execute;

	@CommandLine.Option(names = { "-f" }, description = "Forces the reload of the sdn list", required = false)
	Boolean sdnforce = false;

	@Override
	public void run() {
		LOG.info("Running ofacSdnAdv " + execute.name());
		try {
			producerTemplate.getCamelContext().addRoutes(routes);

			switch (execute) {
			case fetch:
				producerTemplate.sendBody(OFacSdnAdvancedRoutes.ROUTE_SDN_FETCH_FTP, null);
				break;
			case populate:
				// TODO:
				break;
			default:
				break;
			}
		} catch (Exception e) {
			LOG.error("Failed to run Command", e);
		}
	}
}

@ApplicationScoped
class OFacSdnAdvancedRoutes extends RouteBuilder {
	public static final String ROUTE_SDN_FETCH_FTP = "direct:ofacSdnAdv_sdnFetchFtp";

	@Override
	public void configure() throws Exception {

		from(OFacSdnAdvancedRoutes.ROUTE_SDN_FETCH_FTP).pollEnrich(
				"ftp://{{ofac.ftp.host}}:{{ofac.ftp.port}}/fac_sdn?startScheduler=false&binary=true&fileName=sdn_advanced.zip")
				.split(new ZipSplitter()) //
				.streaming() //
				.to("log:DEBUG?showBody=false&showHeaders=true").to("file:{{begui.ofac.sdn.directory}}") //
				.end() //
				.log("Wrote file to disk");
	}

}

package com.begui.ofac.picocli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.camel.AggregationStrategy;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jpa.JpaHelper;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.jboss.logging.Logger;

import com.begui.ofac.db.sdn.Program;
import com.begui.ofac.db.sdn.PublishInfo;
import com.begui.ofac.db.sdn.SdnType;
import com.begui.ofac.picocli.mapper.SdnJaxbToEntityMapper;
import com.begui.ofac.sdn.SdnList;

import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "ofacSdn", mixinStandardHelpOptions = true)
public class OFacSdnCommand implements Runnable {

	public static final Logger LOG = Logger.getLogger(OFacSdnCommand.class);

	@Inject
	ProducerTemplate producerTemplate;

	@Inject
	CommandLine.IFactory factory;

	@Inject
	OFacSdnRoutes routes;

	enum ExecuteType {
		fetch, populate, fetchAndPopulate
	}

	@CommandLine.Option(names = { "-e" }, description = "Possible Values: ${COMPLETION-CANDIDATES}", required = true)
	ExecuteType execute;

	@CommandLine.Option(names = { "-f" }, description = "Forces the reload of the sdn list", required = false)
	Boolean sdnforce = false;

	@Override
	public void run() {
		LOG.info("Running ofacSdn " + execute.name());
		try {
			producerTemplate.getCamelContext().addRoutes(routes);

			switch (execute) {
			case fetch:
				producerTemplate.sendBody(OFacSdnRoutes.ROUTE_SDN_FETCH_FTP, null);
				break;
			case populate:
				producerTemplate.sendBodyAndProperty(OFacSdnRoutes.ROUTE_SDN_POPULATE, null, "sdnforce", sdnforce);
				break;
			default:
				producerTemplate.sendBodyAndProperty(OFacSdnRoutes.ROUTE_SDN_RELOAD, null, "sdnforce", sdnforce);
				break;
			}
		} catch (Exception e) {
			LOG.error("Failed to run Command", e);
		}
	}
}

@ApplicationScoped
class OFacSdnRoutes extends RouteBuilder {
	public static final String ROUTE_SDN_RELOAD = "direct:ofacSdn_sdnReload";
	public static final String ROUTE_SDN_FETCH_FTP = "direct:ofacSdn_sdnFetchFtp";
	public static final String ROUTE_SDN_POPULATE = "direct:ofacSdn_sdnPopulate";

	private static final String ROUTE_JAXB_TO_JPA_SDN = "direct:sdnjaxbToJpa";

	@Inject
	SdnJaxbToEntityMapper mapper;

	@Inject
	@PersistenceUnit
	EntityManagerFactory entityManagerFactory;

	@Override
	public void configure() throws Exception {

//@formatter:off
		//
		// This Route will pull the sdn_xml.zip file, unmarshal, unzip it and send it off to get persisted to the database via
		// the 'ROUTE_JAXB_TO_JPA_SDN' route
		//
		from(OFacSdnRoutes.ROUTE_SDN_RELOAD) //
				.log("Starting " + OFacSdnRoutes.ROUTE_SDN_RELOAD) //
				.pollEnrich("ftp://{{ofac.ftp.host}}:{{ofac.ftp.port}}/fac_sdn?startScheduler=false&binary=true&passiveMode=true&fileName=sdn_xml.zip")
				.unmarshal() //
				.zipFile() //
				.to(OFacSdnRoutes.ROUTE_JAXB_TO_JPA_SDN);
		//
		// This Route will pull the sdn_xml.zip file, unmarshal, unzip it and save it into the local directory 'begui.ofac.sdn.directory'
		// This defaults to '/tmp'
		//
		from(OFacSdnRoutes.ROUTE_SDN_FETCH_FTP) //
				.log("Starting " + OFacSdnRoutes.ROUTE_SDN_FETCH_FTP) //
				.pollEnrich("ftp://{{ofac.ftp.host}}:{{ofac.ftp.port}}/fac_sdn?startScheduler=false&binary=true&passiveMode=true&fileName=sdn_xml.zip")
				.unmarshal() //
				.zipFile() //
				.to("file:{{begui.ofac.sdn.directory}}") //
				.log("Wrote file to disk");
		//
		// This Route will read the sdn.xml file located in 'begui.ofac.sdn.directory' and and send it off to get persisted to the database via
		// the 'ROUTE_JAXB_TO_JPA_SDN' route
		//
		from(OFacSdnRoutes.ROUTE_SDN_POPULATE) //
				.log("Starting " + OFacSdnRoutes.ROUTE_SDN_POPULATE) //
				.pollEnrich("file:{{begui.ofac.sdn.directory}}?noop=true&idempotent=false&fileName=sdn.xml") //
				.log("file:{{begui.ofac.sdn.directory}}?fileName=sdn.xml")
				.to(OFacSdnRoutes.ROUTE_JAXB_TO_JPA_SDN);
		//
		// This will take the sdn.xml file and save it to the db
		//
		var jaxb = new JaxbDataFormat();
		jaxb.setContextPath(com.begui.ofac.sdn.SdnList.class.getPackage().getName());
		from(OFacSdnRoutes.ROUTE_JAXB_TO_JPA_SDN) //
				.doTry()
					.choice()
						.when(bodyAs(com.begui.ofac.sdn.SdnList.class).isNotNull())
							.log("Sdn Unmarshaling to jaxb") //
							.unmarshal(jaxb) //
							.log("Sdn Unmarshaled jaxb") //
							.process(e -> {
								// Note: Had an issue using typeConverters. Going with this instead
								// 		https://github.com/apache/camel-quarkus/issues/2260
								final var sdn = e.getIn().getBody(SdnList.class);
								if(sdn != null) {
									sdn.getPublshInformation().getPublishDate();
									var sdnPublishInfo = mapper.toPublishInfo(sdn);
									e.getIn().setHeader("sdnPublishInfoPublishDate", sdnPublishInfo.publishDate);
									e.getIn().setHeader("sdnPublishInfoRecordCount", sdnPublishInfo.recordCount);
									e.getIn().setHeader("sdnForce", e.getProperty("sdnforce"));
									e.getIn().setBody(sdnPublishInfo, PublishInfo.class );
								}
							}) //
							//
							//
							//
							.enrich()
							  .simple("jpa://" + PublishInfo.class.getName() +"?query=select count(p) from "+ PublishInfo.class.getName() +" p where p.publishDate = '${header.sdnPublishInfoPublishDate}' and p.recordCount = ${header.sdnPublishInfoRecordCount}")
								.aggregationStrategy(new AggregationStrategy() {
									@SuppressWarnings("unchecked")
									@Override
									public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
										var body = (ArrayList<Long>)newExchange.getIn().getBody();
										var icount = body == null || body.isEmpty() ?  0 : body.get(0);
										oldExchange.setProperty("sdnPublishInfoCount", icount);
										return oldExchange;
									}
							})
							//
							//
							//
							.to("log:DEBUG?showBody=false&showHeaders=true&showProperties=true")
							.choice()
							.when(simple("${header.sdnForce} == false && ${header.sdnPublishInfoCount} > 0"))
								.log("The the sdn list ${header.sdnPublishInfoPublishDate}-${header.sdnPublishInfoRecordCount} has already been loaded. Use -f to force the reload!!!")
							.otherwise()
								.log("Writing to Sdn DB - This may take some time with the 'force -f' option")
								.process(e -> {
									final var usePassedInEntityManager = true;
									final var useSharedEntityManager = false; //
									final var allowRecreate = true;
									final var camelEntityManager = JpaHelper.getTargetEntityManager(e, entityManagerFactory, usePassedInEntityManager, useSharedEntityManager, allowRecreate);
									camelEntityManager.getTransaction().begin();

									var sdnPublishInfo = e.getIn().getBody(PublishInfo.class );
									final var sdnForce = e.getIn().getHeader("sdnForce", Boolean.class);
									if(sdnForce == true) {
										var pubidtoremove = camelEntityManager.createNamedQuery("PublishInfo.getByPublishDate", PublishInfo.class).setParameter("publishDate", sdnPublishInfo.publishDate).getResultStream().findFirst().orElse(null);
										if(pubidtoremove != null) {
											camelEntityManager.remove(pubidtoremove);
										}
									}
									//
									//
									//
									var sdnTypeHash = new HashMap<String, SdnType>();
									var programsHash = new HashMap<String, Program>();
									for(var entry : sdnPublishInfo.sdnEntry) {
										//sdnType
										var sdnTypeEntity  = sdnTypeHash.containsKey(entry.sdnType.name) ? sdnTypeHash.get(entry.sdnType.name) : camelEntityManager.createNamedQuery("SdnType.findByName", SdnType.class).setParameter("name", entry.sdnType.name).getResultStream().findFirst().orElse(null);
										if(sdnTypeEntity == null) {
											sdnTypeEntity = camelEntityManager.merge(entry.sdnType);
										}
										entry.sdnType = sdnTypeEntity;
										sdnTypeHash.put(sdnTypeEntity.name, sdnTypeEntity);
										//Program
										final var programNames = entry.programs.stream().map(program -> program.name).collect(Collectors.toSet());
										entry.programs.clear();
										for(var programName : programNames) {
											var programEntity = programsHash.containsKey(programName) ? programsHash.get(programName) : camelEntityManager.createNamedQuery("Program.findByName", Program.class).setParameter("name", programName).getResultStream().findFirst().orElse(null);
											if(programEntity == null) {
												programEntity = new Program();
												programEntity.name = programName;
												programEntity = camelEntityManager.merge(programEntity);
											}
											programsHash.put(programEntity.name, programEntity);
											entry.programs.add(programEntity);
										}



									}
									camelEntityManager.flush();
									camelEntityManager.getTransaction().commit();
									camelEntityManager.clear();
								})
								.to("jpa://" + PublishInfo.class.getName() + "?usePersist=false")
								.log("Wrote Sdn to DB")
							.end()
							.transform(constant("Processed"))
					.endChoice()
					//
				.endDoTry();
//@formatter:on

	}

}
